export const environment = { 
  production: true,
  //urlParametros: "https://cors-anywhere.herokuapp.com/http://kaz-inspector.com/servicios/v1/parametros",
  //urlParametros: "http://kaz-inspector.com/servicios/v1/parametros",
  urlParametros: "https://kaz-inspector.com/servicios/v1/parametros",

  //urlLogin: "https://cors-anywhere.herokuapp.com/http://kaz-inspector.com/servicios/v1/login",
  //urlLogin: "http://kaz-inspector.com/servicios/v1/login",
  urlLogin: "https://kaz-inspector.com/servicios/v1/login",

  //urlActualizar: "https://cors-anywhere.herokuapp.com/http://kaz-inspector.com/servicios/v1/login",
  //urlActualizar: "http://kaz-inspector.com/servicios/v1/login",
  urlActualizar: "https://kaz-inspector.com/servicios/v1/login",  

  firebaseConfig: {
    apiKey: "AIzaSyCTavC1gnH7yzY-M1-WC1OmhNWE_PL51LQ",
    authDomain: "prueba6-97781.firebaseapp.com",
    databaseURL: "https://prueba6-97781.firebaseio.com",
    projectId: "prueba6-97781",
    storageBucket: "prueba6-97781.appspot.com",
    messagingSenderId: "718491120622",
    appId: "1:718491120622:web:0be54457d3ff1309cfd4ff"
  },

  urlCargarInfo: "https://kaz-inspector.com/servicios/v1/infogeneral",
  urlCargarNovedad: "https://kaz-inspector.com/servicios/v1/cargarnovedad",
  urlCargarArchivo: "https://kaz-inspector.com/servicios/v1/cargararchivos",
  
};

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ToastController, Platform } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MenuController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { environment } from "../../environments/environment.prod";
import * as firebase from 'firebase';
import { PhotoLibrary } from '@ionic-native/photo-library/ngx';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { async } from '@angular/core/testing';
import { computeStackId } from '@ionic/angular/directives/navigation/stack-utils';


@Injectable({
  providedIn: 'root'
})
export class DbService {

  usuario = "";
  clave = "";
  enToSend = true;
  logeado = {
    usuario: "",
    estado: false
  }

  loading;
  cancelLoading = false;

  data:any;

  newdata: any;
  movilizaciones:any;
  movilizacion:any; movIndex;
  procesos:any;
  proceso:any; proIndex;
  unidades:any;
  unidad:any; uniIndex;
  novedades:any;novIndex;

  partes:any;
  tipoNovedades:any;
  nivelNovedades:any;
  tipoArchivos:any;

  popoverPartes;
  popoverNovedades; 
  popoverNiveles;

  parte:any;
  novedad:any;
  nivel:any;
  obsNovedad;
  fotosNewNovedad:any; fotosNewNovedadIndex;
  kilometraje="";
  nivCombustible="";
  obsInformacion="";
  finalizada = "N";

  novedadSelec:any

  codUnidad="";
  scanCamUse=false;

  imagen;
  imagenUri;

  fotosFoto; 
  fotosArchivo; 
  fotosCotizacion;
  fotosNovedad; 
  foto; fotoIndex;
  tipoFoto;

  unidadesRevisadas:any=[]; 

  constructor(
    public router: Router,
    public http: HttpClient,
    public toastController: ToastController,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public plt: Platform, public storage: Storage,
    public menu: MenuController,
    public barcodeScanner: BarcodeScanner,
    public camera: Camera,
    public photoLibrary: PhotoLibrary,
    public file: File,
    private webview: WebView
  ) {
    firebase.initializeApp(environment.firebaseConfig);
  }

  menuCuenta() {
    this.menu.isOpen('end')
      .then((res) => {
        if (res) {
          this.menu.close('end');
        } else {
          this.menu.open('end');
        }
      })
  }

  verUsuario() {
    this.menu.close('end');
    this.router.navigate(['/usuario']);
  }

  cerrarSesion() {
    this.menu.close('end');
    this.logeado.estado = false;
    this.logeado.usuario = "";
    this.guardarLocal('logState', this.logeado);
    this.router.navigate(['/login']);
  }
  verRecodTasks(){
    this.menu.close('end');
    this.router.navigate(['/record-tasks']);
  }

  guardarLocal(nombre, valor) {
    this.storage.set(nombre, valor);
    console.log(this.storage);
  }


  inicioLogeado(){
    console.log('data: ', this.data);
    this.movilizaciones = this.data.MOVILIZACIONES;
    this.nivelNovedades = this.data.NIVELNOVEDAD;
    this.tipoArchivos = this.data.TIPOARCHIVO;
    if(this.data.UNIDADESREVISADAS){this.unidadesRevisadas = this.data.UNIDADESREVISADAS; }
    if(this.data.RANDOMIDCARGA){this.randomId = this.data.RANDOMIDCARGA; }
    if(this.data.UNIDADESNUEVAS){this.unidadesNuevas = this.data.UNIDADESNUEVAS; }
    this.router.navigate(['/home'],{replaceUrl:true});
  }

  leerLocalDB(){
    return new Promise((resolve, reject) => {    //se llama en app.components
      this.storage.get('logState')
        .then((val: any) => {
          if (val) {
            if (val.estado) {
              this.logeado.estado = true;
              this.logeado.usuario = val.usuario;
              this.storage.get(this.logeado.usuario)
                .then((val) => {
                  this.data = this.limpiarData(val);   //******** */
                  this.inicioLogeado();
                  resolve('');
                })
                .catch((err) => {
                  console.log(err);
                  resolve('');
                });
            }
            else{resolve('');}
          }
          else {
            this.logeado.estado = false;
            this.logeado.usuario = "";
            this.router.navigate(['/login']);
            resolve('');
          }
          console.log('Logeado: ', this.logeado);
        })
        .catch((err) => {
          console.log(err);
          this.router.navigate(['/login']);
          resolve('');
        });
    });
  }

  async presentToast(mensaje,duracion=3000) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: duracion,
      color: "new"
    });
    toast.present();
  }

  async presentAlert(titulo, subtitulo, mensaje) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: mensaje,
      cssClass: "border-radius: 20px;",
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentAlertNov(titulo, subtitulo, mensaje) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: mensaje,
      cssClass: "border-radius: 20px;",
      buttons: [
        {
          text: 'VER INFO',
          handler: (blah) => {
            this.router.navigate(['/record-tasks']);
          }
        },
        {
          text: 'OK',
          handler: (blah) => {
            console.log('Confirm ok:', blah);
          }
        }  
      ]
    });

    await alert.present();
  }


  async presentLoading(mensaje = 'Por favor espera...') {
    this.loading = await this.loadingController.create({
      message: mensaje,
    });
    await this.loading.present();
    if(this.cancelLoading){this.cancelLoading=false; this.loading.dismiss();}
  
    setTimeout(() => { this.loading.dismiss(); }, 5000);   /////para que el loading no se quede pegado en ningun caso

  }


  getParamsDB(){
    let url = environment.urlParametros;
    let body = {
    };
    let HTTPoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data'
      })
    };
    this.http.post(url, body, HTTPoptions)
    .subscribe((res: any) => {
        this.data.NIVELNOVEDAD = res.data.NIVELNOVEDAD;
        this.data.TIPOARCHIVO = res.data.TIPOARCHIVO;
        console.log('Parametros insertados: ',this.data);
        this.guardarLocal(this.data.USER, this.data);
        this.nivelNovedades = this.data.NIVELNOVEDAD;
        this.tipoArchivos = this.data.TIPOARCHIVO;
      },
      (error: any) => {
        console.log('Error: ' + JSON.stringify(error));
        this.presentAlert('Error', 'Acceso denegado', error.error.message);
        this.enToSend = true;
      });
  }


  limpiarData(dataClear){
    console.log('Limpiar data...');
    let cont:boolean;
    do{
      cont = false;
      dataClear.MOVILIZACIONES.forEach(function(movilizacion,indexMov,movilizacionesObj){
        movilizacionesObj[indexMov].PROCESOS.forEach(function(proceso,indexPro,procesosObj){
          if(!procesosObj[indexPro].UNIDADES){
            procesosObj.splice(indexPro,1);
            console.log('proceso borrado porque estaba vacio: ',proceso);
            cont = true;
          }
          if(procesosObj.length == 0){
            movilizacionesObj.splice(indexMov,1);
            console.log('movilizacion borrada porque estaba vacia: ',movilizacion);
            cont = true;
          }
        });
      });
    }while(cont);

    //limpia unidades nuevas
    let allUni = [];dataClear.MOVILIZACIONES.forEach(movilizacion => { movilizacion.PROCESOS.forEach(proceso => { proceso.UNIDADES.forEach(unidad => { 
      unidad.IDMOVILIZACION = movilizacion.IDMOVILIZACION;
      unidad.IDPROCESO = proceso.IDPROCESO;
      allUni.push(unidad); 
    }); }); }); // trae todas las unidades existentes en memoria y las referencia en sus procesos y movilizaciones respectivas
    console.log('todas las unidades: ',allUni);
    if(dataClear.UNIDADESNUEVAS){this.unidadesNuevas = dataClear.UNIDADESNUEVAS; } //trae todas las unidades nuevas en memoria
    do{
      cont = false;
      if(this.unidadesNuevas.length > 0){
        this.unidadesNuevas.forEach((newUni,index) => {
          if(allUni.findIndex(Uni => Uni.VIN === newUni.VIN && Uni.IDPROCESO === newUni.IDPROCESO && Uni.IDMOVILIZACION === newUni.IDMOVILIZACION) == -1){
            this.unidadesNuevas.splice(index,1);
            console.log('unidad borrada de nuevos porque no existe: ',newUni);
            cont = true;
          }
        });
      }
    }while(cont);
    dataClear.UNIDADESNUEVAS = this.unidadesNuevas;

    return dataClear;
  }

  todoNuevo(){
    let unidadesNuevasAnt = this.unidadesNuevas.length;
    let newTasks
    this.movilizaciones.forEach((movilizacion,indexMov) => {
      movilizacion.PROCESOS.forEach((proceso,indexPro) => {
        proceso.UNIDADES.forEach(unidad=> {
          this.unidadesNuevas.push({
            IDMOVILIZACION: movilizacion.IDMOVILIZACION,
            IDPROCESO:proceso.IDPROCESO,
            VIN:unidad.VIN
          })
        });
      });
    });

    this.data.UNIDADESNUEVAS=this.unidadesNuevas;
    newTasks = this.unidadesNuevas.length - unidadesNuevasAnt;
    let taskN = this.unidadesNuevas.length - newTasks;
    if(newTasks > 0){
      if(this.unidadesNuevas.length > 0 && taskN > 0){ this.presentAlertNov('Nuevas tareas!','te han asignado '+newTasks+' tarea(s) nueva(s)','y tienes '+ taskN+' tarea(s) que no has visto'); }
      else{this.presentAlertNov('Nuevas tareas!','te han asignado '+newTasks+' tarea(s) nueva(s)','');}
    }else{
      if(this.unidadesNuevas.length > 0){this.presentAlertNov('No te han asignado más tareas','','pero tienes '+this.unidadesNuevas.length+' tarea(s) que no has visto');}
      else{this.presentAlertNov('No te han asignado más tareas','','');}
    }
    this.guardarLocal(this.data.USER, this.data);
  }

  newLogin() {
    console.log('Logeo Nuevo');
    let url = environment.urlLogin;
    let body = {
      user: this.usuario,
      password: this.clave
    };
    let HTTPoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data'
      })
    };
    this.http.post(url, body, HTTPoptions)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status == "success") {
          this.data = this.limpiarData(res.data);
          this.data.CLAVE = this.clave;
          console.log(this.data);
          this.movilizaciones = this.data.MOVILIZACIONES;
          this.router.navigate(['/home'],{replaceUrl:true});
          this.logeado.estado = true;
          this.logeado.usuario = this.data.USER;
          this.guardarLocal('logState', this.logeado);
          console.log('Logeado: ', this.logeado);
          this.presentToast('Bienvenido ' + this.data.USERNAME);
          this.getParamsDB();
          this.todoNuevo();
          //this.badgeNewtask()
          this.guardarLocal(this.data.USER, this.data);
        } else {
          console.log(res.message);
        }
        this.enToSend = true;
        this.usuario = "";
        this.clave = "";
        this.loading.dismiss();
      },
      (error: any) => {
        console.log('Error detectado: ' + JSON.stringify(error));
        console.log('mensaje error: ',error.error.message);
        if(error.error.message == undefined){error.error.message = 'Revisa tu conexión a Internet';}
        this.presentAlert('Error', 'Acceso denegado', error.error.message);
        this.enToSend = true;
        this.cancelLoading =  true;
      });
  }

  localLogin() {
    console.log('Logeo local', this.data);
    this.inicioLogeado();
    this.logeado.estado = true;
    this.logeado.usuario = this.data.USER;
    this.guardarLocal('logState', this.logeado);
    console.log('Logeado: ', this.logeado);
    this.presentToast('Bienvenido de nuevo ' + this.data.USERNAME);
    this.enToSend = true;
    this.usuario = "";
    this.clave = "";
    this.cancelLoading =  true;
  }

  login() {
    if (this.enToSend) {
      this.presentLoading();
      this.enToSend = false;
      this.storage.get(this.usuario)
        .then((res) => {
          if (res == null) {
            this.newLogin();
          } else {
            if (res.CLAVE == this.clave) {
              this.data = this.limpiarData(res);
              this.localLogin();
            } else {
              this.presentAlert('Error', 'Acceso denegado', 'Usuario y/o Clave incorrecto');
              this.enToSend = true;
              this.cancelLoading =  true;
            }
          }
        });
    }
  }

  unidadesNuevas:any=[];
  unidadesEliminadas:any[]=[];

  delUnidadesNuevas(vin){
    this.unidadesEliminadas.push(vin);
    let indexN = this.unidadesNuevas.findIndex(uniNew => uniNew.VIN === vin);
    if( indexN > -1){ this.unidadesNuevas.splice(indexN,1); }
  }

  verUniDel(proceso,procesoNew,indexMov,indexPro){
    if(proceso.UNIDADES){
      proceso.UNIDADES.forEach((unidad,indexUn) => {
        if(procesoNew.UNIDADES){
          if(procesoNew.UNIDADES.findIndex(newUni => newUni.VIN === unidad.VIN) == -1){
            let indexdel = this.data.MOVILIZACIONES[indexMov].PROCESOS[indexPro].UNIDADES.findIndex(uni => uni.VIN === unidad.VIN);
            this.data.MOVILIZACIONES[indexMov].PROCESOS[indexPro].UNIDADES.splice(indexdel,1);
            this.delUnidadesNuevas(unidad.VIN);
          }
        }
      });
    }
  }

  verUniNew(proceso,procesoNew,indexMov,indexPro){
    if(proceso.UNIDADES){
      do{
        this.verUniDel(proceso,procesoNew,indexMov,indexPro);
        procesoNew.UNIDADES.forEach(unidadNew => {
          let newUni = true;
          proceso.UNIDADES.forEach(unidad => {
            if(unidadNew.VIN == unidad.VIN){newUni = false;}
          });
          if(newUni){
            this.data.MOVILIZACIONES[indexMov].PROCESOS[indexPro].UNIDADES.push(unidadNew)
            if(this.unidadesNuevas.findIndex(unidad => unidad.VIN === unidadNew.VIN)==-1){
              this.unidadesNuevas.push({
                IDMOVILIZACION: this.data.MOVILIZACIONES[indexMov].IDMOVILIZACION,
                IDPROCESO:proceso.IDPROCESO,
                VIN:unidadNew.VIN
              })
            }
          }
        });
        if(this.data.MOVILIZACIONES[indexMov].PROCESOS[indexPro].UNIDADES.length == 0){ 
          let delIndex = this.data.MOVILIZACIONES[indexMov].PROCESOS.findIndex(proc => proc.IDPROCESO === proceso.IDPROCESO);
          this.data.MOVILIZACIONES[indexMov].PROCESOS.splice(delIndex,1);
        }
        if(this.data.MOVILIZACIONES[indexMov].PROCESOS.length == 0){
          let indexdel = this.data.MOVILIZACIONES.findIndex(mov => mov.IDMOVILIZACION === this.data.MOVILIZACIONES[indexMov].IDMOVILIZACION);
          this.data.MOVILIZACIONES.splice(indexdel,1);
        }
      }while(proceso.UNIDADES.length != procesoNew.UNIDADES.length);
    }
  }

  verProDel(movilizacion, movilizacionNew, indexMov){
      movilizacion.PROCESOS.forEach((proceso,indexPro) => {
        if(movilizacionNew.PROCESOS.findIndex(newPro => newPro.IDPROCESO === proceso.IDPROCESO) == -1){
          proceso.UNIDADES.forEach(unidad => { this.delUnidadesNuevas(unidad.VIN); });
          let delIndex = this.data.MOVILIZACIONES[indexMov].PROCESOS.findIndex(proc => proc.IDPROCESO === proceso.IDPROCESO);
          this.data.MOVILIZACIONES[indexMov].PROCESOS.splice(delIndex,1);
        }
      });
    
  }

  verProcNew(movilizacion, movilizacionNew,indexMov){
    do{
    this.verProDel(movilizacion,movilizacionNew,indexMov);
    movilizacionNew.PROCESOS.forEach((procesoNew,indexProNew) => {
      let newProc = true;
      movilizacion.PROCESOS.forEach((proceso,indexPro) => {
        if(procesoNew.IDPROCESO == proceso.IDPROCESO){newProc = false; this.verUniNew(proceso,procesoNew,indexMov,indexPro)}
      });
      if(newProc){
        this.data.MOVILIZACIONES[indexMov].PROCESOS.push(procesoNew)
        procesoNew.UNIDADES.forEach(unidadNew => {
          if(this.unidadesNuevas.findIndex(unidad => unidad.VIN === unidadNew.VIN)==-1){
            this.unidadesNuevas.push({
              IDMOVILIZACION: movilizacion.IDMOVILIZACION,
              IDPROCESO:procesoNew.IDPROCESO,
              VIN:unidadNew.VIN
            })
          }
        });
      }
    });
    if(this.data.MOVILIZACIONES[indexMov].PROCESOS.length == 0){
      let indexdel = this.data.MOVILIZACIONES.findIndex(mov => mov.IDMOVILIZACION === movilizacion.IDMOVILIZACION);
      this.data.MOVILIZACIONES.splice(indexdel,1);
    }
    }while(movilizacion.PROCESOS.length != movilizacionNew.PROCESOS.length)
  }

  verMovDel(){
      this.data.MOVILIZACIONES.forEach((movilizacion,indexMov) => {
        if(this.newdata.MOVILIZACIONES.findIndex(newMov => newMov.IDMOVILIZACION === movilizacion.IDMOVILIZACION) == -1){
          movilizacion.PROCESOS.forEach(proceso => { proceso.UNIDADES.forEach(unidad => { this.delUnidadesNuevas(unidad.VIN); }); });
          let indexdel = this.data.MOVILIZACIONES.findIndex(mov => mov.IDMOVILIZACION === movilizacion.IDMOVILIZACION);
          this.data.MOVILIZACIONES.splice(indexdel,1);
        }
      });
      console.log('mov')
    
  }

  verMovNew(){
    this.unidadesEliminadas=[];
    let unidadesNuevasAnt = this.unidadesNuevas.length;
    let newTasks
    do{
      this.verMovDel(); 
      this.newdata.MOVILIZACIONES.forEach( (movilizacionNew,indexMovNew) => {
        let newMov = true;
        this.data.MOVILIZACIONES.forEach( (movilizacion,indexMov) => {
          if(movilizacionNew.IDMOVILIZACION == movilizacion.IDMOVILIZACION){newMov=false; this.verProcNew(movilizacion, movilizacionNew, indexMov);}
        });
        if(newMov){
          this.data.MOVILIZACIONES.push(movilizacionNew);
          movilizacionNew.PROCESOS.forEach((proceso,indexPro) => {
            if(proceso.UNIDADES){
              proceso.UNIDADES.forEach(unidadNew => {
                if(this.unidadesNuevas.findIndex(unidad => unidad.VIN === unidadNew.VIN)==-1){
                  this.unidadesNuevas.push({
                    IDMOVILIZACION: movilizacionNew.IDMOVILIZACION,
                    IDPROCESO:proceso.IDPROCESO,
                    VIN:unidadNew.VIN
                  })
                }
              });
            }
          });
        }
      });
    } while(this.data.MOVILIZACIONES.length != this.newdata.MOVILIZACIONES.length);
    this.data.UNIDADESNUEVAS=this.unidadesNuevas;
    newTasks = this.unidadesNuevas.length - unidadesNuevasAnt;
    let taskN = this.unidadesNuevas.length - newTasks;
    if(newTasks > 0){
      if(this.unidadesNuevas.length > 0 && taskN > 0){ this.presentAlertNov('Nuevas tareas!','te han asignado '+newTasks+' tarea(s) nueva(s)','y tienes '+ taskN+' tarea(s) que no has visto'); }
      else{this.presentAlertNov('Nuevas tareas!','te han asignado '+newTasks+' tarea(s) nueva(s)','');}
    }else{
      if(this.unidadesNuevas.length > 0){this.presentAlertNov('No te han asignado más tareas','','pero tienes '+this.unidadesNuevas.length+' tarea(s) que no has visto');}
      else{this.presentAlertNov('No te han asignado más tareas','','');}
    }
    if(this.unidadesEliminadas.length > 0){
      this.presentAlert('Tarea(s) eliminada(s)','','Se ha(n) eliminado '+this.unidadesEliminadas.length+' tarea(s)')
    }
  }

  badgeNewtask(){
    if(this.movilizaciones){
      this.movilizaciones.forEach((movilizacion,indexMov) => {
        let nTasks = 0; movilizacion.PROCESOS.forEach(proceso => { 
          if(proceso.UNIDADES){
            proceso.UNIDADES.forEach(unidad => {nTasks+=1; }); 
          }
        });
        this.movilizaciones[indexMov].NTASKS=nTasks;
        let news = this.unidadesNuevas.filter(uniNew => uniNew.IDMOVILIZACION === movilizacion.IDMOVILIZACION)
        this.movilizaciones[indexMov].NEWS=news.length;
      });
    }
  }


  actualizar() {
    if (this.enToSend) {
      this.enToSend = false;
      this.presentLoading();
      let url = environment.urlActualizar;
      let body = {
      user: this.data.USER,
      password: this.data.CLAVE
    };
    let HTTPoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data'
      })
    };
    this.http.post(url, body, HTTPoptions)
      .subscribe(async (res: any) => {
          console.log(res);
          if (res.status == "success") {
            console.log('New data: ', res.data);
            this.newdata = this.limpiarData(res.data);
            console.log('clear data: ',this.newdata);
            this.verMovNew();
            this.badgeNewtask();
            this.guardarLocal(this.data.USER, this.data);
          } else {
            console.log(res.message);
          }
          this.loading.dismiss();
          this.enToSend = true;
        },
        (error: any) => {
          console.log('Error detectado: ' + JSON.stringify(error));
          console.log('mensaje error: ',error.error.message);
          if(error.error.message == undefined){error.error.message = 'Revisa tu conexión a Internet';}
          this.presentAlert('Error', 'Acceso denegado', error.error.message);
          this.enToSend = true;
          this.cancelLoading =  true;
        });
    }
  }

  actualizarPru(){
    this.menu.close('end');
    if (this.enToSend) {
      this.enToSend = false;
      this.presentLoading('Descargando prueba...');
      console.log('Actualizando');
      firebase.database().ref(this.data.USER).once('value')
        .then((snap)=>{
          console.log('datos de firebase: ',snap.val());
          this.data = snap.val();
          this.inicioLogeado();
          this.badgeNewtask();
          this.guardarLocal(this.data.USER, this.data);
          this.movilizaciones = this.data.MOVILIZACIONES;
          this.loading.dismiss();
          this.enToSend = true;
        })
        .catch((err)=>{
          console.log('error: ',err);
          this.loading.dismiss();
          this.enToSend = true;
        })
    }
  }

  subirAdb() {
    this.menu.close('end');
    if (this.enToSend) {
    this.enToSend = false;
    this.presentLoading();
    firebase.database().ref(this.data.USER).set(this.data)
      .then((res)=>{
        console.log('guardado en firebase ',res);
        this.loading.dismiss();
        this.enToSend = true;
      })
      .catch((err)=>{
        console.log('error: ',err);
        this.loading.dismiss();
        this.enToSend = true;
      })
    }
  }

  irProcesos(movilizacion) {
    this.movIndex = this.movilizaciones.indexOf(movilizacion);
    console.log('index movilizacion: ',this.movIndex);
    this.movilizacion = movilizacion;
    this.procesos = this.movilizacion.PROCESOS;
    this.partes = this.movilizacion.PARTES;
    this.tipoNovedades = this.movilizacion.TIPONOVEDAD;
    console.log('moviliacion: ',this.movilizacion)
    console.log('procesos: ',this.procesos);
    console.log('partes: ', this.partes);
    console.log('tipo de novedades: ',this.tipoNovedades);
    this.router.navigate(['/procesos']);
  }

  verUnidadesFinalizadas(){
    this.unidades.forEach((value,index) => {
      if(!value.INFORMACION){
        this.unidades[index].INFORMACION = {
          FINALIZADA: "N"
        }
      }
    });
  }

  irUnidades(proceso) {
    this.proIndex = this.procesos.indexOf( proceso);
    console.log('index proceso: ',this.proIndex);
    this.proceso = proceso;
    this.unidades = this.proceso.UNIDADES;
    this.verUnidadesFinalizadas();
    console.log('proceso: ',this.proceso);
    console.log('unidades: ',this.unidades);
    this.router.navigate(['/unidades']);
  }

  irUnidad(unidad) {
    this.uniIndex = this.unidades.indexOf(unidad);
    console.log('index unidad: ',this.uniIndex);
    this.unidad = unidad;
    this.parte = {NOMBREPARTE:"Selecciona"};
    this.novedad = {NOMBRETIPONOVEDAD:"Selecciona"};
    this.nivel = {NOMBRE:"Selecciona"};
    this.obsNovedad = "";
    this.finalizada = unidad.INFORMACION.FINALIZADA;
    if(unidad.INFORMACION.KILOMETRAJE){this.kilometraje = unidad.INFORMACION.KILOMETRAJE;}else{this.kilometraje = "";}
    if(unidad.INFORMACION.NIVELCOMBUSTIBLE){this.nivCombustible = unidad.INFORMACION.NIVELCOMBUSTIBLE;}else{this.nivCombustible = "";}
    if(unidad.INFORMACION.OBSERVACIONES){this.obsInformacion = unidad.INFORMACION.OBSERVACIONES;}else{this.obsInformacion = "";}
    //if(unidad.FINALIZADA){this.finalizada = unidad.INFORMACION.FINALIZADA;}else{this.finalizada = "N";}
    console.log(this.unidad);
    this.fotosNewNovedad = null;
    this.router.navigate(['/unidad']);
  }

  filtrarArchivos(){
    if(this.unidad.ARCHIVOS){
      this.fotosArchivo = this.unidad.ARCHIVOS.filter( fotos => fotos.IDTIPOARCHIVO.includes('1') );
      this.fotosFoto = this.unidad.ARCHIVOS.filter( fotos => fotos.IDTIPOARCHIVO.includes('2') );
      this.fotosCotizacion = this.unidad.ARCHIVOS.filter( fotos => fotos.IDTIPOARCHIVO.includes('4') );
    }else{
      this.fotosFoto = null;
      this.fotosArchivo = null;
      this.fotosCotizacion =  null;
    }
  }

  filtrarFotosNovedad(){
    if(this.unidad.NOVEDADES[this.novIndex].FOTOS){
      this.fotosNovedad = this.unidad.NOVEDADES[this.novIndex].FOTOS;
    }else{
      this.fotosNovedad = null;
    }
  }

  irFotos() {
    this.filtrarArchivos();
    this.router.navigate(['/fotos']);
  }

  irNovedades() {
    this.router.navigate(['/novedades']);
  }

  irArchivo() {
    this.filtrarArchivos();
    this.router.navigate(['/archivo']);
  }

  irCotizacion() {
    this.filtrarArchivos();
    this.router.navigate(['/cotizacion']);
  }

  irInformacion() {
    this.router.navigate(['/informacion']);
  }

  scanCamera(){
    this.barcodeScanner.scan().then(barcodeData => {
      this.scanCamUse = true;
      console.log('Barcode data', barcodeData);
      this.codUnidad = barcodeData.text;
     }).catch(err => {
         console.log('Error', err);
     });
  }

  guardarArchivo(tipo){
    if(tipo == '0'){
      if(this.unidad.NOVEDADES[this.novIndex].FOTOS){
        this.unidad.NOVEDADES[this.novIndex].FOTOS.push({
            ARCHIVO: this.imagen,
            URI: this.imagenUri
        });
      }else{
        let newFoto:any[]=[];
        newFoto.push({
            ARCHIVO: this.imagen,
            URI: this.imagenUri
        });
        this.unidad.NOVEDADES[this.novIndex].FOTOS = newFoto;
      }
      this.guardarLocal(this.data.USER, this.data);
      this.filtrarFotosNovedad();
    }else{
      let archivo = {
        IDMOVILIZACION: this.movilizacion.IDMOVILIZACION,
        IDPROCESO: this.proceso.IDPROCESO,
        IDUNIDAD: this.unidad.IDUNIDAD,
        IDCARGA: this.genRandomIdCarga(),
        IDTIPOARCHIVO: tipo,
        ARCHIVO: this.imagen,
        URI: this.imagenUri
      }
      console.log('archivo: ',archivo);
      if(this.unidad.ARCHIVOS){
        this.unidad.ARCHIVOS.push(archivo);
      }else{
        let primerArchivo:any = [];
        primerArchivo.push(archivo);
        this.unidad.ARCHIVOS = primerArchivo;
      }
      this.guardarLocal(this.data.USER, this.data);
      this.filtrarArchivos();
    }
  }

  tomarFoto(tipo){
    const options: CameraOptions = {
      quality: 50,
      targetHeight: 1280,
      targetWidth: 960,
      destinationType: this.camera.DestinationType.FILE_URI,
      //sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      // allowEdit: true,
      //saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then((imageData) => {
      console.log('lista la foto', imageData);
      this.photoLibrary.requestAuthorization().then(() => {
        console.log('aqui');
        // console.log('saveImage: ',res);
        this.imagen = imageData;
        this.imagenUri = this.imagen;
        this.imagen = this.webview.convertFileSrc(this.imagen);
        this.guardarArchivo(tipo);
        // this.photoLibrary.saveImage(imageData,'kaz')
        //   .then((res)=>{
        //     console.log('saveImage: ',res);
        //     this.imagen ='file://' + res.id.split(';')[1];
        //     this.imagenUri = this.imagen;
        //     this.imagen = this.webview.convertFileSrc(this.imagen);
        //     this.guardarArchivo(tipo);
        //   });
      })
      .catch(err => console.log('permissions weren\'t granted'));

    }, (err) => {
      console.log('error: ',err);
    });
  }

  abrirFoto(tipo){
    const options: CameraOptions = {
      //quality: 50,
      targetHeight: 1280,
      targetWidth: 960,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      // allowEdit: true,
      // correctOrientation: true
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then((imageData) => {
      imageData = imageData.substring(0,imageData.lastIndexOf('?'))
      console.log('lista la foto desde el archivo: ', imageData);
      this.photoLibrary.requestAuthorization().then(() => {
        console.log(imageData);
            this.imagen = imageData;
            this.imagenUri = this.imagen;
            this.imagen = this.webview.convertFileSrc(this.imagen);
            this.guardarArchivo(tipo);
        // this.photoLibrary.saveImage(imageData,'kaz')
        //   .then((res)=>{
        //     console.log('saveImage: ',res);
        //     this.imagen ='file://' + res.id.split(';')[1];
        //     this.imagenUri = this.imagen;
        //     this.imagen = this.webview.convertFileSrc(this.imagen);
        //     this.guardarArchivo(tipo);
        //   })
      })
      .catch(err => console.log('permissions weren\'t granted'));

    }, (err) => {
      console.log('error: ',err);
    });
  }

  guardarFotoNewNovedad(){
    if(this.fotosNewNovedad){ 
      this.fotosNewNovedad.push({
        ARCHIVO: this.imagen,
        URI: this.imagenUri
      });}
      else{
        let newFoto:any[] = [];
        newFoto.push({
          ARCHIVO: this.imagen,
          URI: this.imagenUri
        });
        this.fotosNewNovedad = newFoto;
      }
  }

  tomarFotoNewNovedad(){
    const options: CameraOptions = {
      quality: 50,
      targetHeight: 1280,
      targetWidth: 960,
      destinationType: this.camera.DestinationType.FILE_URI,
      //sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      // allowEdit: true,
      //saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then((imageData) => {
      console.log('lista la foto', imageData);
      this.photoLibrary.requestAuthorization().then(() => {
        console.log('saveImage: ',imageData);
            this.imagen = imageData;
            this.imagenUri = this.imagen;
            this.imagen = this.webview.convertFileSrc(this.imagen);
            this.guardarFotoNewNovedad();
        // this.photoLibrary.saveImage(imageData,'kaz')
        //   .then((res)=>{
        //     console.log('saveImage: ',res);
        //     this.imagen ='file://' + res.id.split(';')[1];
        //     this.imagenUri = this.imagen;
        //     this.imagen = this.webview.convertFileSrc(this.imagen);
        //     this.guardarFotoNewNovedad();
        //   });
      })
      .catch(err => console.log('permissions weren\'t granted'));
    }, (err) => {
      console.log('error: ',err);
    });
  }

  abrirFotoNewNovedad(){
    const options: CameraOptions = {
      //quality: 50,
      targetHeight: 1280,
      targetWidth: 960,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      // allowEdit: true,
      // correctOrientation: true
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then((imageData) => {
      imageData = imageData.substring(0,imageData.lastIndexOf('?'))
      console.log('lista la foto desde el archivo: ', imageData);


      this.photoLibrary.requestAuthorization().then(() => {
        console.log('permisos aceptados');
        console.log('saveImage: ',imageData);
            this.imagen = imageData;
            this.imagenUri = this.imagen;
            this.imagen = this.webview.convertFileSrc(this.imagen);
            this.guardarFotoNewNovedad();
        // this.photoLibrary.saveImage(imageData,'kaz')
        //   .then((res)=>{
        //     console.log('saveImage: ',res);
        //     this.imagen ='file://' + res.id.split(';')[1];
        //     this.imagenUri = this.imagen;
        //     this.imagen = this.webview.convertFileSrc(this.imagen);
        //     this.guardarFotoNewNovedad();
        //   })
        //   .catch((res)=>console.log('error: ',res));
      })
      .catch(err => console.log('permissions weren\'t granted'));
    }, (err) => {
      console.log('error: ',err);
    });
  }

  revisionTerminada = false;
  guardarInformacion(){
    //console.log('nivel combustible: ', this.nivCombustible);
    if( this.nivCombustible!=""){ // &&  this.kilometraje!=""   ){
      let informacion = {
        IDMOVILIZACION: this.movilizacion.IDMOVILIZACION,
        IDPROCESO: this.proceso.IDPROCESO,
        IDUNIDAD: this.unidad.IDUNIDAD,
        KILOMETRAJE: this.kilometraje,
        NIVELCOMBUSTIBLE: this.nivCombustible,
        OBSERVACIONES: this.obsInformacion,
        IDCARGA: this.genRandomIdCarga(),
        FINALIZADA: this.finalizada,
        USUARIO: this.data.USER
      }
      //console.log('Informacion general: ', informacion);
      this.data.MOVILIZACIONES[this.movIndex].PROCESOS[this.proIndex].UNIDADES[this.uniIndex].INFORMACION=informacion;
      if(this.finalizada == "S"){
        this.showcarga = true;
        this.revisionTerminada = true; // se llama a addUnRev() en la pagina unidades en ionviewdidenter
        this.presentLoading('Preparando para enviar'); ////****** */
        this.router.navigate(['/unidades']);
        //this.addUnRev();
      }else{
        this.guardarLocal(this.data.USER, this.data);
        this.router.navigate(['/unidad']);
        this.presentToast('Información guardada');
      }
    }else{
      this.presentAlert('Error','Datos incompletos','los campos con * son obligatorios');
    }

  }

  agregarNovedad(){
    if(this.parte.IDPARTE && this.nivel.IDNIVELNOVEDAD && this.novedad.IDTIPONOVEDAD){
      let novedad = {
        IDMOVILIZACION: this.movilizacion.IDMOVILIZACION,
        IDPROCESO: this.proceso.IDPROCESO,
        IDUNIDAD: this.unidad.IDUNIDAD,
        PARTE: this.parte,
        NIVEL: this.nivel,
        NOVEDAD: this.novedad,
        OBSERVACIONES: this.obsNovedad,
        IDCARGA: this.genRandomIdCarga(),
        FOTOS: this.fotosNewNovedad
      };
      console.log("novedad: ",novedad);
      if(this.data.MOVILIZACIONES[this.movIndex].PROCESOS[this.proIndex].UNIDADES[this.uniIndex].NOVEDADES == null){
        let iniArray:any[] = [];
        iniArray.push(novedad)
        this.data.MOVILIZACIONES[this.movIndex].PROCESOS[this.proIndex].UNIDADES[this.uniIndex].NOVEDADES=iniArray;
      }
      else{this.data.MOVILIZACIONES[this.movIndex].PROCESOS[this.proIndex].UNIDADES[this.uniIndex].NOVEDADES.push(novedad);}
      this.guardarLocal(this.data.USER, this.data);
      this.fotosNewNovedad = null;
      this.parte = {NOMBREPARTE:"Selecciona"};
      this.novedad = {NOMBRETIPONOVEDAD:"Selecciona"};
      this.nivel = {NOMBRE:"Selecciona"};
      this.obsNovedad = "";
      this.presentToast('Novedad Agregada',1000);
    }else{
      this.presentAlert('Error','Datos incompletos','Por favor completa los datos con *');
    }
  }

  irNovedad(novedad){
    //this.novIndex = this.unidad.NOVEDADES.findIndex( nov => nov === novedad);
    this.novIndex = this.unidad.NOVEDADES.indexOf(novedad)
    console.log('index unidad: ',this.novIndex);
    this.novedadSelec = novedad
    console.log('Novedad seleccionada: ',this.novedadSelec);
    this.filtrarFotosNovedad();
    this.router.navigate(['/novedad']);
  }

  async borrarNovedad() {
    const alert = await this.alertController.create({
      header: 'Alerta!',
      message: 'Aceptas eliminar esta novedad?',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.data.MOVILIZACIONES[this.movIndex].PROCESOS[this.proIndex].UNIDADES[this.uniIndex].NOVEDADES.splice(this.novIndex,1);
            console.log(this.data.MOVILIZACIONES[this.movIndex].PROCESOS[this.proIndex].UNIDADES[this.uniIndex].NOVEDADES);
            this.guardarLocal(this.data.USER, this.data);
            this.presentToast('Novedad eliminada',1000);
            this.router.navigate(['/novedades']);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel:', blah);
          }
        } 
      ]
    });

    await alert.present();
  }

  irFoto(foto,tipArchivo,newFoto=false){
    if(tipArchivo == '0'){
      if(newFoto){ this.tipoFoto='newnovedad'; this.foto = this.fotosNewNovedad; this.fotoIndex = this.fotosNewNovedad.indexOf(foto);}
      else{this.tipoFoto='novedad'; this.foto = this.fotosNovedad; this.fotoIndex = this.fotosNovedad.indexOf(foto);}
    }
    if(tipArchivo == '1'){ this.tipoFoto='archivo';
      //this.foto = []; this.fotosArchivo.forEach(foto => { this.foto.push(foto.ARCHIVO) });
      this.foto = this.fotosArchivo;
      this.fotoIndex = this.fotosArchivo.indexOf(foto); 
    }
    if(tipArchivo == '2'){this.tipoFoto='foto'; 
      //this.foto = []; this.fotosFoto.forEach(foto => { this.foto.push(foto.ARCHIVO) });
      this.foto = this.fotosFoto; 
      this.fotoIndex = this.fotosFoto.indexOf(foto); 
    }
    if(tipArchivo == '4'){ this.tipoFoto='cotizacion';
      //this.foto = []; this.fotosCotizacion.forEach(foto => { this.foto.push(foto.ARCHIVO) }); 
      this.foto = this.fotosCotizacion;
      this.fotoIndex = this.fotosCotizacion.indexOf(foto); 
    }
    this.router.navigate(['/foto']);
  }


  async borrarFoto(index) {
    const alert = await this.alertController.create({
      header: 'Alerta!',
      message: 'Aceptas eliminar esta foto?',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            if(this.tipoFoto == 'newnovedad'){
              this.fotosNewNovedad.splice(index,1);
              this.foto = this.fotosNewNovedad;
              this.router.navigate(['/novedades']);
            }else if(this.tipoFoto == 'novedad'){
              let Index = this.unidad.NOVEDADES[this.novIndex].FOTOS.indexOf(this.foto[index]);
              this.unidad.NOVEDADES[this.novIndex].FOTOS.splice(Index,1);
              this.filtrarFotosNovedad();
              this.router.navigate(['/novedad']);
            }else{
              let Index;
              if(this.tipoFoto == 'archivo'){ Index = this.unidad.ARCHIVOS.indexOf(this.fotosArchivo[index]); this.router.navigate(['/archivo']); }
              if(this.tipoFoto == 'foto'){ Index = this.unidad.ARCHIVOS.indexOf(this.fotosFoto[index]); this.router.navigate(['/fotos']); }
              if(this.tipoFoto == 'cotizacion'){ Index = this.unidad.ARCHIVOS.indexOf(this.fotosCotizacion[index]); this.router.navigate(['/cotizacion']); }
              this.unidad.ARCHIVOS.splice(Index,1);
              this.filtrarArchivos();
            }
            this.guardarLocal(this.data.USER, this.data);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel:', blah);
          }
        } 
      ]
    });

    await alert.present();
  }

  randomId=[];

  genRandomIdCarga(){
    let num = 0;
    let rev = true;
    let min = 1000;
    let max = 9999;
    while(rev){
      num = Math.floor(Math.random() * (max - min)) + min,
      rev = this.randomId.includes(num);
    }
    this.randomId.push(num);
    this.data.RANDOMIDCARGA = this.randomId;
    return num;
  }

  deleteIdCarga(id){
    let index = this.randomId.indexOf(id);
    this.randomId.splice(index,1);
    this.data.RANDOMIDCARGA = this.randomId;
  }

  filtObs(obs){ if(obs==""){obs=".";}return obs; }

  insertarInformacion(unidad){
    let informacion = {
      IDMOVILIZACION: unidad.IDMOVILIZACION,
      IDPROCESO: unidad.IDPROCESO,
      IDUNIDAD: +unidad.IDUNIDAD,
      IDCARGA: unidad.IDCARGA,
      KILOMETRAJE: +unidad.KILOMETRAJE,
      NIVELCOMBUSTIBLE: unidad.NIVELCOMBUSTIBLE,
      OBSERVACIONES: this.filtObs(unidad.OBSERVACIONES),
      FINALIZADA: unidad.FINALIZADA,
      USUARIO: unidad.USUARIO
    }
    return informacion;
  }

  async convertirA64(foto){
    let filename = foto.substring(foto.lastIndexOf('/')+1);
    let path =  foto.substring(0,foto.lastIndexOf('/')+1);
    await this.file.readAsDataURL(path, filename).then(res=> { 
      return res;
    });
  }

  /* waitPush(obj1,obj2){
    return new Promise((res,rej)=>{
      let interval = setInterval(() => {
        console.log('*');
        if(obj1 == obj2 ){
          clearInterval(interval);
          res();
        }
      }, 100);
    });
  } */

  insertarNovedad(novedad){
    let novReady;
    let imagenes:any[]=[];
    if(novedad.FOTOS){
      let numFoto = 0;
      novedad.FOTOS.forEach(async(foto, indexFoto, array) => {
        //imagenes.push({ NombreImagen: this.convertirA64(foto.URI) });
        let filename = foto.URI.substring(foto.URI.lastIndexOf('/')+1);
        let path =  foto.URI.substring(0,foto.URI.lastIndexOf('/')+1);
        this.file.readAsDataURL(path, filename).then( async (res)=> { 
          imagenes.push({ 
            nombre: "Imagen " + numFoto,
            imagen: res
          });
          numFoto +=1;
          if(imagenes.length == array.length){
            novReady = {
              IDMOVILIZACION: novedad.IDMOVILIZACION,
              IDPROCESO: novedad.IDPROCESO,
              IDUNIDAD: +novedad.IDUNIDAD,
              IDPARTE: +novedad.PARTE.IDPARTE,
              IDNIVEL: +novedad.NIVEL.IDNIVELNOVEDAD,
              IDTIPONOVEDAD: +novedad.NOVEDAD.IDTIPONOVEDAD,
              OBSERVACIONES: novedad.OBSERVACIONES,
              IDCARGA: novedad.IDCARGA,
              IMAGENES: imagenes
            }
            await new Promise((resolve)=>{
              let interval = setInterval(() => {
                console.log('*');
                if(novReady.IMAGENES == imagenes){
                  clearInterval(interval);
                  resolve('');
                }
              }, 1000);
            });
            console.log('novedad: ',novReady);
            this.enviarServidor(environment.urlCargarNovedad,novReady);
          }
        });
      });
    }else{
      novReady = {
        IDMOVILIZACION: novedad.IDMOVILIZACION,
        IDPROCESO: novedad.IDPROCESO,
        IDUNIDAD: +novedad.IDUNIDAD,
        IDPARTE: +novedad.PARTE.IDPARTE,
        IDNIVEL: +novedad.NIVEL.IDNIVELNOVEDAD,
        IDTIPONOVEDAD: +novedad.NOVEDAD.IDTIPONOVEDAD,
        OBSERVACIONES: novedad.OBSERVACIONES,
        IDCARGA: novedad.IDCARGA,
        IMAGENES: imagenes
      }
      //console.log('novedad: ',novReady);
      this.enviarServidor(environment.urlCargarNovedad,novReady);
    }
  }

  insertarArchivo(archivo){
    let filename = archivo.URI.substring(archivo.URI.lastIndexOf('/')+1);
    let path =  archivo.URI.substring(0,archivo.URI.lastIndexOf('/')+1);
    this.file.readAsDataURL(path, filename).then((res)=> {
      let archivoReady = {
        IDMOVILIZACION: archivo.IDMOVILIZACION,
        IDPROCESO: archivo.IDPROCESO,
        IDUNIDAD: +archivo.IDUNIDAD,
        IDTIPOARCHIVO: +archivo.IDTIPOARCHIVO,
        IDCARGA: archivo.IDCARGA,
        ARCHIVO: res
      }
      //console.log('Archivo: ',archivoReady);
      this.enviarServidor(environment.urlCargarArchivo,archivoReady);
    });
  }

  guardarUnidRev(){
    this.data.UNIDADESREVISADAS=this.unidadesRevisadas;
    this.guardarLocal(this.data.USER, this.data);
  }

  noNull(objeto){
    console.log('arreglo vacio');
    if(!objeto){objeto = [];}
    return objeto;
  }

  addUnRev(){
    let newUniRev = {
      IDMOVILIZACION: this.movilizacion.IDMOVILIZACION,
      IDPROCESO: this.proceso.IDPROCESO,
      IDUNIDAD: this.unidad.IDUNIDAD,
      INFORMACION: this.unidad.INFORMACION,
      NOVEDADES: this.noNull(this.unidad.NOVEDADES),
      ARCHIVOS: this.noNull(this.unidad.ARCHIVOS)
    };
    if(this.unidadesRevisadas.length == 0){
      this.unidadesRevisadas.push(newUniRev);
    }else{
      let ind = this.unidadesRevisadas.findIndex(uni => uni.IDUNIDAD == this.unidad.IDUNIDAD);
      if(ind > -1){
        this.unidadesRevisadas[ind] = newUniRev;
      }else{
        this.unidadesRevisadas.push(newUniRev);
      }
    }
    this.cargarUnidadesRev(); 
    console.log('unidades revisadas: ',this.unidadesRevisadas);
    //this.guardarUnidRev();
  }

  verUnidadAborrar(unidad,indexU){
    if(this.unidadesRevisadas[indexU].INFORMACION == "cargado" && this.unidadesRevisadas[indexU].NOVEDADES.length == 0 && this.unidadesRevisadas[indexU].ARCHIVOS.length == 0){
      let indexMovDel = this.data.MOVILIZACIONES.findIndex(mov => mov.IDMOVILIZACION === unidad.IDMOVILIZACION);
      let indexProcDel = this.data.MOVILIZACIONES[indexMovDel].PROCESOS.findIndex(proc => proc.IDPROCESO === unidad.IDPROCESO);
      let indexUnDel = this.data.MOVILIZACIONES[indexMovDel].PROCESOS[indexProcDel].UNIDADES.findIndex(uni => uni.IDUNIDAD === unidad.IDUNIDAD);
      console.log("se eliminará la unidad: ",indexUnDel);
      console.log("del proceso: ",indexProcDel);
      console.log("de la movilizacion: ",indexMovDel);
      this.unidadesRevisadas.splice(indexU,1);
      this.data.MOVILIZACIONES[indexMovDel].PROCESOS[indexProcDel].UNIDADES.splice(indexUnDel,1);
      if(this.data.MOVILIZACIONES[indexMovDel].PROCESOS[indexProcDel].UNIDADES.length == 0){this.data.MOVILIZACIONES[indexMovDel].PROCESOS.splice(indexProcDel,1);}
      if(this.data.MOVILIZACIONES[indexMovDel].PROCESOS.length == 0){this.data.MOVILIZACIONES.splice(indexMovDel,1);}
    }
    this.guardarUnidRev();
  }

  buscarParaBorrar(idcarga){
    this.unidadesRevisadas.forEach((unidad,indexU) => {
      if(unidad.INFORMACION.IDCARGA == idcarga){this.unidadesRevisadas[indexU].INFORMACION = "cargado" ; this.deleteIdCarga(idcarga); this.verUnidadAborrar(unidad,indexU); return;}
      unidad.NOVEDADES.forEach((novedad,indexN) => {
        if(novedad.IDCARGA == idcarga){ this.unidadesRevisadas[indexU].NOVEDADES.splice(indexN,1);this.deleteIdCarga(idcarga); this.verUnidadAborrar(unidad,indexU); return; }
      });
      unidad.ARCHIVOS.forEach((archivo,indexN) => {
        if(archivo.IDCARGA == idcarga){ this.unidadesRevisadas[indexU].ARCHIVOS.splice(indexN,1);this.deleteIdCarga(idcarga);this.verUnidadAborrar(unidad,indexU); return; }
      });
    });
  }

  numCargas = 0;
  maxNumCargas = 0;
  porcentajeCarga = 0;
  showcarga=false;
  showPorcentaje = 0;
  enviarServidor2(url,body){
    console.log('url HTTP: ',url);
    console.log('body HTTP: ',body);
  }  
  enviarServidor(url,body){
    console.log('url HTTP: ',url);
    console.log('body HTTP: ',body);
    this.numCargas+=1;this.maxNumCargas+=1;this.showcarga=true
    let HTTPoptions = {
      headers: new HttpHeaders({
        //'Content-Type': 'application/json',
        //'Content-Type': 'text/plain',
        'Content-Type': 'multipart/form-data',
        //'Authorization': 'dea1d5786420947a0f0ab43d7dead68d'
      })
    };
    this.http.post(url, body, HTTPoptions)
      .subscribe((res: any) => {
        console.log('respuesta del servidor: ',res);
        this.buscarParaBorrar(res.IDCARGA);
        this.numCargas-=1;
        if(this.maxNumCargas>0){
          this.porcentajeCarga = (this.maxNumCargas - this.numCargas)/this.maxNumCargas;
          this.showPorcentaje = Math.floor(this.porcentajeCarga*100);
        }
        if(this.numCargas == 0){
          this.guardarUnidRev();
          //this.loading.dismiss();
          console.log('unidades revisadas: ', this.unidadesRevisadas);
          if(this.unidadesRevisadas.length == 0){this.presentToast('Perfecto, se ha cargado todo!');}
          else{this.presentToast('Quedaron '+ this.unidadesRevisadas.length  +' unidad(es) por cargar');}
          this.enToSend=true;
          this.showcarga = false;
          this.porcentajeCarga = 0;
        }
      },
      (error: any) => {
        console.log('Error: ' + JSON.stringify(error));
        if(error.error.message == undefined){
          error.error.message = 'Revisa tu conexión a Internet'; 
          //this.cancelLoading=true;
        }
        if(this.internet){this.presentAlert('Error', 'mensaje:', error.error.message);}
        if(error.error.message == 'Revisa tu conexión a Internet'){this.internet=false;}
        this.numCargas-=1;
        if(this.numCargas == 0){
          this.guardarUnidRev();
          //this.loading.dismiss();
          console.log('unidades revisadas: ', this.unidadesRevisadas);
          this.presentToast('Quedaron '+ this.unidadesRevisadas.length  +' unidad(es) por cargar');
          this.enToSend=true;
          this.showcarga = false;
          this.porcentajeCarga = 0;
        }
      });
  }

  internet = true;
  cargarUnidadesRev(){
    //this.presentLoading('Cargando Unidades revisadas, elementos: '+this.numCargas);
    console.log('unidades revisadas: ',this.unidadesRevisadas)
    if(this.enToSend){
      this.enToSend=false;
      if(this.unidadesRevisadas.length > 0){
        this.numCargas = 0;
        this.maxNumCargas = 0;
        this.internet = true;
        if(this.loading){this.loading.dismiss();}  ///**** */
        this.unidadesRevisadas.forEach(unidad => {
          if(unidad.INFORMACION != "cargado"){this.enviarServidor(environment.urlCargarInfo,this.insertarInformacion(unidad.INFORMACION));}
          if(unidad.NOVEDADES){if(unidad.NOVEDADES.length > 0){
            unidad.NOVEDADES.forEach(novedad => {
              this.insertarNovedad(novedad);
            });
          }}
          if(unidad.ARCHIVOS){if(unidad.ARCHIVOS.length > 0){
            unidad.ARCHIVOS.forEach(archivo => {
              this.insertarArchivo(archivo);
            });
          }}
        });
      }else{this.presentToast('No tienes datos para cargar');this.enToSend=true;}
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { DbService } from "../services/db.service";

@Component({
  selector: 'app-unidades',
  templateUrl: './unidades.page.html',
  styleUrls: ['./unidades.page.scss'],
})
export class UnidadesPage implements OnInit {

  unidadesFil:any;
  badgeEn = false;

  constructor( public db: DbService ) { 
    this.busquedaUnidad();
  }

  ngOnInit() {}
 

  busquedaUnidad(){
    console.log(this.db.codUnidad);
    if(this.db.codUnidad == ""){
      this.badgeEn=false;
      this.unidadesFil = this.db.unidades;
    }else{
      this.badgeEn=true;
      this.unidadesFil = this.db.unidades.filter( unidades => unidades.VIN.toUpperCase().includes(this.db.codUnidad.toUpperCase()) );
      if(this.db.scanCamUse){
        this.db.scanCamUse = false;
        if(this.unidadesFil.length == 1){this.db.irUnidad(this.unidadesFil[0]);}
        else{this.db.presentAlert('Alerta',this.db.codUnidad,'No se encontró ningún resultado!');this.db.codUnidad = "";}
      }
    }
    //console.log('unidades Filtradas: ',this.unidadesFil);
  }

  ionViewDidEnter(){
    if(this.db.revisionTerminada){this.db.revisionTerminada=false; this.db.addUnRev();}///esto viene de service cuando una revision fue terminada
    this.db.unidades.forEach((unidad,indexUn) => {
      let news = this.db.unidadesNuevas.filter(uniNew => ( uniNew.VIN === unidad.VIN && uniNew.IDPROCESO === this.db.proceso.IDPROCESO && uniNew.IDMOVILIZACION === this.db.movilizacion.IDMOVILIZACION) );
      this.db.unidades[indexUn].NEWS=news.length;
    });
  }

  ionViewWillLeave(){
    this.db.codUnidad = "";
  }

}

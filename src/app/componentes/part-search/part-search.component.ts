import { Component, OnInit } from '@angular/core';
import { DbService } from "../../services/db.service";

@Component({
  selector: 'app-part-search',
  templateUrl: './part-search.component.html',
  styleUrls: ['./part-search.component.scss'],
})
export class PartSearchComponent implements OnInit {

  partFil;
  bagEn=false;

  constructor(public db:DbService) { 
    this.db.partes.sort((a,b) => {
      if(+a.CODIGOPARTE > +b.CODIGOPARTE){return 1;}
      if(+a.CODIGOPARTE < +b.CODIGOPARTE){return -1;}
      return 0;
    });
    this.busquedaParte("");
  }
  
  ngOnInit() {
  }

  busquedaParte(ev){
    console.log(ev);
    if(ev == ""){
      this.bagEn = false;
      this.partFil = this.db.partes;
    }else{
      this.bagEn = true;
      this.partFil = this.db.partes.filter( partes => partes.NOMBREPARTE.toUpperCase().includes(ev.toUpperCase()) || partes.CODIGOPARTE.toUpperCase().includes(ev.toUpperCase()) );
    }
    //console.log('partes Filtradas: ',this.partFil);
  }

  selectParete(parte){
    this.db.parte = parte;
    console.log(this.db.parte);
    this.db.popoverPartes.dismiss();
  }

}

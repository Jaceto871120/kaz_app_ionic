import { Component, OnInit } from '@angular/core';
import { DbService } from "../../services/db.service";

@Component({
  selector: 'app-niv-search',
  templateUrl: './niv-search.component.html',
  styleUrls: ['./niv-search.component.scss'],
})
export class NivSearchComponent implements OnInit {

  constructor(public db:DbService) { }

  ngOnInit() {}

  selectNiv(nivel){
    this.db.nivel = nivel;
    console.log(this.db.nivel);
    this.db.popoverNiveles.dismiss();
  }

}

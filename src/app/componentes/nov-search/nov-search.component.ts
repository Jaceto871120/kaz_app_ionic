import { Component, OnInit } from '@angular/core';
import { DbService } from "../../services/db.service";

@Component({
  selector: 'app-nov-search',
  templateUrl: './nov-search.component.html',
  styleUrls: ['./nov-search.component.scss'],
})
export class NovSearchComponent implements OnInit {

  novFil;
  bagEn=false;

  constructor(public db:DbService) {
    this.busquedaNovedad("");
  } 

  ngOnInit() {}

  busquedaNovedad(ev){
    console.log(ev);
    if(ev == ""){
      this.bagEn = false;
      this.novFil = this.db.tipoNovedades;
    }else{
      this.bagEn = true;
      this.novFil = this.db.tipoNovedades.filter( novedades => novedades.NOMBRETIPONOVEDAD.toUpperCase().includes(ev.toUpperCase()) || novedades.CODIGOTIPONOVEDAD.toUpperCase().includes(ev.toUpperCase()) );
    }
    //console.log('partes Filtradas: ',this.partFil);
  }

  selectNov(novedad){
    this.db.novedad = novedad;
    console.log(this.db.novedad);
    this.db.popoverNovedades.dismiss();
  }

}

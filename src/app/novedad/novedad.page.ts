import { Component, OnInit } from '@angular/core';
import { DbService } from "../services/db.service";

@Component({
  selector: 'app-novedad',
  templateUrl: './novedad.page.html',
  styleUrls: ['./novedad.page.scss'],
})
export class NovedadPage implements OnInit {

  constructor(public db:DbService) { }

  ngOnInit() {
  }

}

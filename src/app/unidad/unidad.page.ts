import { Component, OnInit } from '@angular/core';
import { DbService } from "../services/db.service";

@Component({
  selector: 'app-unidad',
  templateUrl: './unidad.page.html',
  styleUrls: ['./unidad.page.scss'],
})
export class UnidadPage implements OnInit {

  constructor( public db: DbService) { }

  ngOnInit() {
    let indexNew = this.db.unidadesNuevas.findIndex(uniNew => uniNew.VIN === this.db.unidad.VIN && uniNew.IDPROCESO === this.db.proceso.IDPROCESO && uniNew.IDMOVILIZACION === this.db.movilizacion.IDMOVILIZACION); //******************** */
    if(indexNew > -1){
      this.db.unidadesNuevas.splice(indexNew,1);
      console.log('esta unidad ya no es nueva');
      this.db.data.UNIDADESNUEVAS = this.db.unidadesNuevas;
      this.db.guardarLocal(this.db.data.USER, this.db.data);
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { DbService } from "../services/db.service";

@Component({
  selector: 'app-procesos',
  templateUrl: './procesos.page.html',
  styleUrls: ['./procesos.page.scss'],
})
export class ProcesosPage implements OnInit {

  constructor( public db:DbService ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.db.procesos.forEach((proceso,indexProc) => {
      let nTasks = 0; proceso.UNIDADES.forEach(unidad => {nTasks+=1; });
      this.db.procesos[indexProc].NTASKS=nTasks;
      let news = this.db.unidadesNuevas.filter(uniNew => (uniNew.IDPROCESO === proceso.IDPROCESO && uniNew.IDMOVILIZACION === this.db.movilizacion.IDMOVILIZACION ) );
      this.db.procesos[indexProc].NEWS=news.length;
    });
  }

}

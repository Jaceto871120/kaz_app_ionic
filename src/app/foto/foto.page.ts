import { Component, OnInit, ViewChild } from '@angular/core';
import { DbService } from "../services/db.service";
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-foto',
  templateUrl: './foto.page.html',
  styleUrls: ['./foto.page.scss'],
})
export class FotoPage implements OnInit {

  @ViewChild(IonSlides, {static:true}) mySlides: IonSlides

  constructor(public db: DbService) { }

  slideOpts = {
    initialSlide: this.db.fotoIndex,
    zoom:{
      maxRatio: 5,
      minRatio: 1
    }

  };

  ngOnInit() {
    
  }

  borrar(){
    this.mySlides.getActiveIndex()
      .then((index)=>{
        console.log('Index: ', index);
        this.db.borrarFoto(index);
      });

  }

}

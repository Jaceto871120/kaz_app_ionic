import { Component, OnInit } from '@angular/core';
import { DbService } from "../services/db.service";
import { PopoverController, Platform } from '@ionic/angular';
import { PartSearchComponent } from "../componentes/part-search/part-search.component";
import { NovSearchComponent } from "../componentes/nov-search/nov-search.component";
import { NivSearchComponent } from "../componentes/niv-search/niv-search.component";

@Component({
  selector: 'app-novedades',
  templateUrl: './novedades.page.html',
  styleUrls: ['./novedades.page.scss'],
})
export class NovedadesPage implements OnInit {

  el; 

  constructor(public db: DbService, public popoverController: PopoverController, public plt:Platform) { }

  ngOnInit() {
  }

  async PopoverPartes() {
    this.db.popoverPartes = await this.popoverController.create({
      component: PartSearchComponent,
      cssClass: 'popoverStyle',
      mode: 'ios',
      translucent: false,
      animated: true,
      showBackdrop: true,
    });
    this.el = document.querySelector('.popoverStyle');
    this.el.style.setProperty('--width', '80%');
    this.el.style.setProperty('--max-height', '95%');
    return await this.db.popoverPartes.present();
  }

  async PopoverNovedades() {
    this.db.popoverNovedades = await this.popoverController.create({
      component: NovSearchComponent,
      cssClass: 'popoverStyle',
      mode: 'ios',
      translucent: false,
      animated: true,
      showBackdrop: true,
    });
    this.el = document.querySelector('.popoverStyle');
    this.el.style.setProperty('--width', '80%');
    this.el.style.setProperty('--max-height', '95%');
    return await this.db.popoverNovedades.present();
  }

  async PopoverNiveles() {
    this.db.popoverNiveles = await this.popoverController.create({
      component: NivSearchComponent,
      cssClass: 'popoverStyle',
      mode: 'ios',
      translucent: false,
      animated: true,
      showBackdrop: true,
    });
    this.el = document.querySelector('.popoverStyle');
    this.el.style.setProperty('--width', '80%');
    this.el.style.setProperty('--max-height', '95%');
    return await this.db.popoverNiveles.present();
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NovedadesPageRoutingModule } from './novedades-routing.module';

import { NovedadesPage } from './novedades.page';

import { PartSearchComponent } from "../componentes/part-search/part-search.component";
import { NovSearchComponent } from "../componentes/nov-search/nov-search.component";
import { NivSearchComponent } from "../componentes/niv-search/niv-search.component";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NovedadesPageRoutingModule
  ],
  declarations: [NovedadesPage,PartSearchComponent, NovSearchComponent, NivSearchComponent],
  entryComponents: [PartSearchComponent, NovSearchComponent, NivSearchComponent]
})
export class NovedadesPageModule {}

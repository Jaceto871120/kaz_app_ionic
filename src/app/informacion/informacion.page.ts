import { Component, OnInit } from '@angular/core';
import { DbService } from "../services/db.service";

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.page.html',
  styleUrls: ['./informacion.page.scss'],
})
export class InformacionPage implements OnInit {

  constructor(public db:DbService) { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import { DbService } from "../services/db.service";

@Component({
  selector: 'app-archivo',
  templateUrl: './archivo.page.html',
  styleUrls: ['./archivo.page.scss'],
})
export class ArchivoPage implements OnInit {

  constructor(public db: DbService) { }

  ngOnInit() {
  }

}

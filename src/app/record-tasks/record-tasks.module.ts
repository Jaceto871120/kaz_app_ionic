import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecordTasksPageRoutingModule } from './record-tasks-routing.module';

import { RecordTasksPage } from './record-tasks.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecordTasksPageRoutingModule
  ],
  declarations: [RecordTasksPage]
})
export class RecordTasksPageModule {}

import { Component, OnInit } from '@angular/core';
import { DbService } from "../services/db.service";

@Component({
  selector: 'app-record-tasks',
  templateUrl: './record-tasks.page.html',
  styleUrls: ['./record-tasks.page.scss'],
})
export class RecordTasksPage implements OnInit {

  constructor(public db:DbService) { }

  ngOnInit() {
  }

}
